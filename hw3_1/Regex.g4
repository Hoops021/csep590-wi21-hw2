grammar Regex;

re
    : expression EOF
    | alt EOF
    ;

alt
    : expression ('|' expression)* # AlternativeElement
    ;

expression
    : (elems+=element)*
    ;

element
    : a=atom            # AtomicElement
    | a=atom m=MODIFIER # ModifiedElement
    | '(' alt ')' # ParenthesisElement
    | '(' alt ')' m=MODIFIER # ModifiedParenthesisElement
    ;

atom
    : c=CHARACTER
    ;

CHARACTER : [a-zA-Z0-9.] ;
MODIFIER : [?*] ;
WS : [ \t\r\n]+ -> skip ;
