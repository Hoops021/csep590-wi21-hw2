/* Q is a small query language for JavaScript.
 *
 * Q supports simple queries over a JSON-style data structure,
 * kinda like the functional version of LINQ in C#.
 *
 */

// This class represents all AST Nodes.

interface JoinPredicate {
    (datum: any): boolean;
    fieldJoin: string;
}

export const emptyString = "";

export class ASTNode {
    public readonly type: string;

    constructor(type: string) {
        this.type = type;
    }

    execute(data: any[]): any {
        throw new Error("Execute not implemented for " + this.type + " node.");
    }

    optimize(): ASTNode {
        return this;
    }

    run(data: any[]): any {
        return this.optimize().execute(data);
    }

    //// 1.5 implement call-chaining

    filter(predicate: (datum: any) => boolean): ASTNode {
        return new ThenNode(this, new FilterNode(predicate));
    }

    apply(callback: (datum: any) => any): ASTNode {
        return new ThenNode(this, new ApplyNode(callback));
    }

    count(): ASTNode {
        return new ThenNode(this, new CountNode());
    }

    // Extra credit
    unique(callback: (datum: any) => any): ASTNode {
        return new ThenNode(this, new UniqueNode(callback));
    }

    product(query: ASTNode): ASTNode {
        return new CartesianProductNode(this, query);
    }

    join(query: ASTNode, relation: string | ((left: any, right: any) => any)): ASTNode {
        var filter: (left: any, right: any) => any;

        if (typeof(relation) === "string") {
            filter = (l,r) => {
                if (relation in l && relation in r) {
                    return l[relation] === r[relation];
                }
                return false;
            }
        } else {
            filter = relation;
        }

        let predicate = (x => filter(x.left, x.right)) as JoinPredicate;
        predicate.fieldJoin = typeof(relation) === "string" ? relation : emptyString;

        return new ThenNode(
            new ThenNode(
                new CartesianProductNode(this, query),
                new FilterNode(predicate)
            ),
            new ApplyNode(x => Object.assign({}, x.left, x.right)) // P = left, Q = right; break ties by picking Q
        );
    }

    // Extra Credit
    sort(comparator: (a: any, b: any) => number): ASTNode {
        return new ThenNode(this, new SortByNode(comparator));
    }

    // Extra credit
    group_by(g: (datum: any) => any): ASTNode {
        return new GroupByNode(g, new IdNode());
    }

    // Extra credit
    reduce(f: (x: any, y: any) => any, i: any): ASTNode {
        return new ReduceNode(new IdNode(), f, i);
    }

    toString(): string {
        return `${this.type} Node\n`;
    }

    debug() {
        console.log(this.toString());
    }
}

// The Id node just outputs all records from input.
export class IdNode extends ASTNode {

    constructor() {
        super("Id");
    }

    //// 1.1 implement execute
    execute(data: any[]): any {
        if (data == null) return [];

        // Shallow copy only assumption
        return Array.from(data);
    }
}

// We can use an Id node as a convenient starting point for
// the call-chaining interface.
export const Q = new IdNode();

// The Filter node uses a callback to throw out some records.
export class FilterNode extends ASTNode {
    predicate: (datum: any) => boolean;

    constructor(predicate: (datum: any) => boolean) {
        super("Filter");
        this.predicate = predicate;
    }

    //// 1.1 implement execute
    execute(data: any[]): any {
        if (data == null) return [];

        var results = [];
        var self = this;
        data.forEach(function (value) {
            if (self.predicate(value)) {
                results.push(value)
            }
        });

        return results;
    }

    toString(): string {
        return `Filter Node (${this.predicate})\n`;
    }
}

// The Then node chains multiple actions on one data structure.
export class ThenNode extends ASTNode {
    first: ASTNode;
    second: ASTNode;

    constructor(first: ASTNode, second: ASTNode) {
        super("Then");
        this.first = first;
        this.second = second;
    }

    //// 1.1 implement execute
    execute(data: any[]): any {
        return this.second.execute(this.first.execute(data));
    }

    optimize(): ASTNode {
        return new ThenNode(this.first.optimize(), this.second.optimize());
    }

    toString(): string {
        return "Then Node\n" 
            + `\t${this.first.toString()}\n`
            + `\t${this.second.toString()}\n`;
    }
}

//// 1.3 implement Apply and Count Nodes

export class ApplyNode extends ASTNode {
    f: (datum: any) => any;
    constructor(callback: (datum: any) => any) {
        super("Apply");
        this.f = callback;
    }

    execute(data: any[]): any {
        if (data == null) return [];

        var results = [];
        var self = this;
        data.forEach(function (value) {
            results.push(self.f(value));
        });

        return results;
    }

    toString(): string {
        return `Apply Node (${this.f})`;
    }
}

export class CountNode extends ASTNode {
    constructor() {
        super("Count");
    }

    execute(data: any[]): any {
        if (data == null) return 0;
        return [data.length];
    }
}

//// 2.1 optimize queries

// This function permanently adds a new optimization function to a node type.
// An optimization function takes in a node and either returns a new,
// optimized node or null if no optimizations can be performed. AddOptimization
// will register this function to a particular node type, so that it will be called
// as part of that node's optimize() method, along with all other registered
// optimizations.
function AddOptimization(nodeType, opt: (this: ASTNode) => ASTNode | null) {
    const oldOptimize = nodeType.prototype.optimize;
    nodeType.prototype.optimize = function(this: ASTNode): ASTNode {
        const newThis = oldOptimize.call(this);
        return opt.call(newThis) || newThis;
    };
}

// For example, the following small optimization removes unnecessary Id nodes.
AddOptimization(ThenNode, function(this: ThenNode): ASTNode {
    if (this.first instanceof IdNode) {
        return this.second;
    } else if (this.second instanceof IdNode) {
        return this.first;
    } else {
        return null;
    }
});

// The above optimization has a few notable side effects. First, it removes the
// root IdNode from your call chain, so if you were depending on that to exist
// your other optimizations may be confused. Also, it returns the interesting
// subtree directly, without trying to optimize it more. It is up to you to
// determine where additional optimization should occur.

// We won't specifically test for this optimization, so if it's giving you
// a headache feel free to comment it out.

// You can add optimizations for part 2.1 here (or anywhere below).
// ...

AddOptimization(ThenNode, function(this: ThenNode): ASTNode {
    // Return true if this is ThenNode(ThenNode(x, FilterNode(f)),....)
    var IsThenThenFilter: boolean = 
        this.first instanceof ThenNode && this.first.second instanceof FilterNode

    //
    // Filter merge optimization
    //

    // Then(Then(x, Filter(f)), Filter(g)) => Then(x, Filter(f && g))
    if (IsThenThenFilter && this.second instanceof FilterNode) {
        var subThenNode: ThenNode = this.first as ThenNode;
        var x = subThenNode.first;
        var f = (subThenNode.second as FilterNode).predicate;
        var g = this.second.predicate;

        this.first = x;
        this.second = new FilterNode(f && g);

        return this;
    }
    // Then(Filter(f), Filter(g)) => Filter(f && g)
    else if (this.first instanceof FilterNode 
        && this.second instanceof FilterNode) {

        var f = this.first.predicate;
        var g = this.second.predicate;

        return new FilterNode(f && g);
    }

    // 
    // CountIf optimization
    //

    // Then(Then(x, Filter(f)), Count) => Then(x, CountIf(f))
    if (IsThenThenFilter
        && this.second instanceof CountNode) {
        var subThenNode: ThenNode = this.first as ThenNode;
        var x = subThenNode.first;
        var f = (subThenNode.second as FilterNode).predicate;
        this.first = x;
        this.second = new CountIfNode(f);
        return this;
    } 
    // Then(Filter(f), Count) => CountIf(f)
    else if (this.first instanceof FilterNode 
        && this.second instanceof CountNode) {
        var f = this.first.predicate;
        return new CountIfNode(f);
    }

    // 
    // Join Node optimizations
    //

    // Then(Then(CartesianProduct(l, r), Filter(f)), Apply(g)) => Join(l, r, f)
    if (IsThenThenFilter
        && (this.first as ThenNode).first instanceof CartesianProductNode
        && this.second instanceof ApplyNode) {

        var subThenNode = (this.first as ThenNode);
        var productNode = (subThenNode.first as CartesianProductNode);
        var f = (subThenNode.second as FilterNode).predicate;
        let joinPredicate = (f as JoinPredicate);

        // Only process this pattern if created by fluent methods
        // i.e we provide the predicate as a JoinPredicate type
        if (joinPredicate != null && joinPredicate.fieldJoin != null) {
            if (joinPredicate.fieldJoin !== emptyString) {
                return new HashJoinNode(joinPredicate.fieldJoin, productNode.left, productNode.right);
            }

            return new JoinNode(productNode.left, productNode.right, f);
        }
    }
});


//// 2.2 internal node types and CountIf

export class CountIfNode extends ASTNode {
    predicate: (datum: any) => boolean;
    constructor(predicate: (datum: any) => boolean) {
        super("CountIf");
        this.predicate = predicate;
    }

    execute(data: any[]): any {
        if (data == null) return 0;

        var counter = 0;
        var self = this;
        data.forEach(function (value) {
            if (self.predicate(value)) {
                counter++;
            }
        });

        return counter;
    }

    toString(): string {
        return `CountIf Node - ${this.predicate}`;
    }
}

//// 3.1 cartesian products

export class CartesianProductNode extends ASTNode {
    left: ASTNode;
    right: ASTNode;
    constructor(left: ASTNode, right: ASTNode) {
        super("CartesianProduct");
        this.left = left;
        this.right = right;
    }

    execute(data: any[]): any {
        let leftData = this.left.execute(data);
        let rightData = this.right.execute(data);
        
        let product = [];
        for (var l of leftData) {
            for (var r of rightData) {
                product.push({left: l, right: r});
            }
        }

        return product;
    }

    optimize(): ASTNode {
        return new CartesianProductNode(this.left.optimize(), this.right.optimize());
    }
}

//// 3.2-3.6 joins and hash joins

/* Hint: Recall from the spec that a join of two arrays P and Q by a
   function f(l, r) is the array with one entry for each pair of a record
   in P and Q for which f returns true and that each entry should have all
   fields and values that either record in the pair had.

   Most notably, if both records had the same field, use the value from the
   record from Q. In JavaScript, this can be achieved with Object.assign:
   https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign
*/

export class JoinNode extends ASTNode {
    public left: ASTNode;
    public right: ASTNode;
    public joinFilter: (datum: any) => boolean;

    constructor(left: ASTNode, right: ASTNode, filter: (datum: any) => boolean) {
        super("Join");
        this.left = left;
        this.right = right;
        this.joinFilter = filter;
    }

    execute(data: any[]): any {
        let leftData = this.left.execute(data);
        let rightData = this.right.execute(data);

        let results = [];
        for (var l of leftData) {
            for (var r of rightData) {
                if (this.joinFilter({left:l, right:r})) {
                    results.push(Object.assign({}, l, r));
                }
            }
        }

        return results;
    }

    optimize(): ASTNode {
        return new JoinNode(this.left.optimize(), this.right.optimize(), this.joinFilter);
    }
}

/* Hint: While optimizing the fluent join to use your internal JoinNode,
   if you find yourself needing to compare two functions for equality,
   you can do so with the Javascript operator `===` (pointer equality).
 */

export class HashJoinNode extends ASTNode {
    private left: ASTNode;
    private right: ASTNode;
    private key: string;

    constructor(key: string, left: ASTNode, right: ASTNode) {
        super("HashJoin");
        this.left = left;
        this.right = right;
        this.key = key;
    }

    execute(data: any[]): any {
        let leftData = this.left.execute(data);
        let rightData = this.right.execute(data);
        let leftMap = HashJoinNode.createHashTable(leftData, this.key);
        let rightMap = HashJoinNode.createHashTable(rightData, this.key);

        let results = [];
        for (let [key, leftRows] of leftMap) {
            if (rightMap.has(key)) {
                let rightRows = rightMap.get(key);
                for (let l of leftRows.values()) {
                    for (let r of rightRows.values()) {
                        results.push(Object.assign({}, l, r));
                    }
                }
            }
        }

        return results;
    }

    private static createHashTable(data: any[], key: string): Map<any, any[]> {
        let table = new Map<any, any[]>();
        for (let row of data) {
            let x = row[key];
            if (!table.has(x)) {
                table.set(x, []);
            }

            table.get(x).push(row);
        }

        return table;
    }

    optimize(): ASTNode {
        return new HashJoinNode(this.key, this.left.optimize(), this.right.optimize());
    }
}


// This matches l.join(r, "fieldname") after optimizing into JoinNode
AddOptimization(JoinNode, function(this: JoinNode): ASTNode {
    let joinPredicate = this.joinFilter as JoinPredicate;
    if (joinPredicate.fieldJoin !== emptyString) {
        return new HashJoinNode(joinPredicate.fieldJoin, this.left, this.right);
    }

    return null;
});


// Extra Credit
export class SortByNode extends ASTNode {

    comparator: (a: any, b: any) => number;

    constructor(comparator: (a: any, b: any) => number) {
        super("SortBy");
        this.comparator = comparator;
    }

    execute(data: any[]): any {
        var results = Array.from(data);
        results.sort(this.comparator);
        return results;
    }
}

// Internal node to enable multiple table inputs into a Query
// On execute, just returns shallow copy of table data provided in constructor instead of input
export class TableNode extends ASTNode {
    private table: any[];

    constructor(data:any[]) {
        super("Table");
        this.table = data;
    }

    execute(data:any[]): any[] {
        return Array.from(this.table);
    }
}

// Extra credit
export class UniqueNode extends ASTNode {
    private callback: (datum: any) => any;

    constructor(callback: (datum: any) => any) {
        super("Unique");
        this.callback = callback;
    }

    execute(data: any[]): any[] {
        let s = {}
        for (let x of data) {
            let k = this.callback(x);
            s[k] = x;
        }

        let ans = [];
        for (let k in s) {
            ans.push(s[k]);
        }

        return ans;
    }
}

// Extra credit
export class GroupByNode extends ASTNode {
    private g: (datum: any) => any;
    private p: ASTNode;
    constructor(g: (datum: any) => any, p: ASTNode) {
        super("Group");
        this.g = g;
        this.p = p;
    }

    execute(data: any[]): any[] {
        let hash = new Map<any, any[]>();
        for (let row of data) {
            let key = this.g(row);
            if (!hash.has(key)) {
                hash.set(key, []);
            }
            hash.get(key).push(row);
        }

        let out = [];
        for (let [key, value] of hash) {
            out.push({group: key, records: value});
        }

        return out;
    }
}

// Extra credit
export class ReduceNode extends ASTNode {
    private p: ASTNode;
    private f: (x: any, y: any) => any;
    private i: any;

    constructor(p: ASTNode, f: (x: any, y: any) => any, i: any) {
        super("Reduce");
        this.p = p;
        this.f = f;
        this.i = i;
    }

    execute(data: any[]): any {
        if (data.length == 0) {
            return [];
        }

        let out = this.f(this.i, data[0]);
        for (let i = 1; i < data.length; i++) {
            out = this.f(out, data[i]);
        }

        return out;
    }
}

// Instead of using Q to start a query chain, we denote T as a function.
export const T = (data:any[]) => new TableNode(data);