import * as q from "./q";
const Q = q.Q;

//// 1.2 write a query
export const theftsQuery = new q.ThenNode(new q.IdNode(), new q.FilterNode(x => x[2].includes("THEFT")));
export const autoTheftsQuery = new q.ThenNode(new q.IdNode(), new q.FilterNode(x => x[2].includes("THEFT") && x[3].includes("VEHICLE THEFT")));

//// 1.4 clean the data

class Crime {
    constructor(
        public description: string,
        public category: string,
        public area: string,
        public date: string,
    ) {}
  }

let cleanupCallback = x => new Crime(x[2], x[3], x[4], x[5]);
export const cleanupQuery = new q.ThenNode(new q.IdNode(), new q.ApplyNode(cleanupCallback));

//// 1.6 reimplement queries with call-chaining

export const cleanupQuery2 = Q.apply(cleanupCallback);
export const theftsQuery2 = Q.filter(x => x.description.includes("THEFT"));
export const autoTheftsQuery2 = Q.filter(x => x.description.includes("THEFT") && x.category.includes("VEHICLE THEFT"));

//// 4 put your queries here (remember to export them for use in tests)

// Filter only to entries that occurred before the year 2000 and sorted in ascending order
export const pre2000Query = Q.apply(cleanupCallback)
    .filter(x => {
        var datetime = new Date(x["date"]);
        var milleniumDate = new Date("01/01/2000");
        return datetime < milleniumDate;
    }).sort((a,b) => new Date(a.date).getTime() - new Date(b.date).getTime() );

let dateQ = Q.apply(cleanupCallback).apply(x => {return {crime: x, dateObj: new Date(x.date)}});
export const consecutiveCrimesPairsSamePlace = dateQ
    .join(dateQ,
        (l, r) => {
        return (l.dateObj.getDate() + 1) % 7 == r.dateObj.getDate() && l.crime.area == r.crime.area;
    })
    .apply(x => x.crime);

// This query search all the windows of 4 consecutive days where there was a crime in seattle.
let areaAndDate = cleanupQuery.apply(x => {return {category: x.category, date: new Date(x.date)}});
export const fourCrimeDays = areaAndDate
    .product(areaAndDate).apply(x => {return {day1: x.left, day2: x.right}})
    .product(areaAndDate).apply(x => Object.assign({}, x.left, {day3: x.right}))
    .product(areaAndDate).apply(x => Object.assign({}, x.left, {day4: x.right}))
    .apply(x => Object.assign({}, x, {dates: [x.day1.date, x.day2.date, x.day3.date, x.day4.date].sort(x => x.getDate())}))
    .filter(x => x.dates[0].getDate() + 1 == x.dates[1].getDate() && x.dates[1].getDate() + 1 == x.dates[2].getDate() && x.dates[2].getDate() + 1 == x.dates[3].getDate() )
    .filter(x => x.day1.date.getYear() == x.day2.date.getYear() && x.day2.date.getYear() == x.day3.date.getYear() && x.day3.date.getYear() == x.day4.date.getYear())
    .apply(x => [x.day1.date.getDate(), x.day2.date.getDate(), x.day3.date.getDate(), x.day4.date.getDate()])
    .unique(x => x.toString());


// Then(Then(CartesianProduct(l, r), Filter(f)), Apply(g)) => Join(l, r, f)
export const fakeJoinQuery = Q.product(areaAndDate).filter(x => x.right.category.includes("THEFT")).apply(x => x.left);