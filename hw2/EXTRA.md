- Sort: This works by declaring a `SortNode` class which simply receives an array and sorts it using the provided comparator and the built-in sort function. An important detail is to deep copy the array to ensure the function is pure.

- Reduce: The implementation in the `ReduceNode` iterates over the array applying the predicate with the previous value and the current value in the array. The intial output is initialized with the callback applied to the initial value of the array and the 'intial row'.

- Group By: The implementation in `GroupByNode` uses a Map to group the keys in the data into subtables which are subsequently transformed into an array of rows with the "group" and "records" fields.

- Multiple tables: this is implemented with a `TableNode` that simply receives data and always return the same data, similar to the IdNode but with the data as an argument, then this node can be passed to any function to introduce another data input. To ease its use, an anonymous function called `T` creates the node based on a given input, similar to the `Q` semantics.

- Unique: this fluent method computes unique rows based on a key which is computed via a predicate. The implementation in `UniqueNode` simply computes the key for each row and assigns them into a dictionary and ultimately return the values.

