import {expect} from "chai";

import * as q from "../src/q";
const Q = q.Q;
const T = q.T;

import * as queries from "../src/queries";

import {crimeData} from "./data";
const rawData = crimeData.data;

// You should provide at least 5 tests, using your own queries from Part 4
// and either the crime data from test/data.ts or your own data. You can look
// at test/q.ts to see how to import and use the queries and data.
// The outline of the first test has been provided as an example - you should
// modify it so it actually does something interesting.


class Stub extends q.ASTNode {
    private data: any[];
    constructor(data: any[]) {
        super("Stub");
        this.data = data;
    }

    //// 1.1 implement execute
    execute(data: any[]): any {
        return this.data;
    }
}

// Duplicated from q.ts provided in test
function flattenThenNodes(query) {
    if (query instanceof q.ThenNode) {
        return flattenThenNodes(query.first).concat(flattenThenNodes(query.second));
    } else if (query instanceof q.IdNode) {
        return [];
    } else {
        return [query];
    }
}

// Duplicated from q.ts provided in test
function expectQuerySequence(query, seq) {
    const flat = flattenThenNodes(query);
    expect(flat.length).to.be.equal(seq.length);
    for (let i = 0; i < flat.length; i++) {
        expect(flat[i].type).to.be.equal(seq[i]);
    }
}

describe("custom", () => {


    it("Crimes before Y2000 sorted", () => {
        const out = queries.pre2000Query.execute(rawData);

        expect(out).to.be.an("Array");
        expect(out).to.have.length(6);

        var dateBoundary = new Date("1/1/2020");
        var lastDate = new Date(0);
        for (let i = 0; i < out.length; i++) {
            var currentDate = new Date(out[i].date);
            expect(currentDate).to.be.lessThan(dateBoundary);
            expect(lastDate).to.be.lessThan(currentDate);
            lastDate = currentDate;
        }
    });
    
    it("Null/Empty Inputs", () => {
        const out = Q.apply(x => x).execute(null);

        expect(out).to.be.an("Array");
        expect(out).to.have.length(0);

        const out2 = Q.filter(x => x[0] > 10).count().execute([]);

        expect(out2).to.be.an("Array");
        expect(out2[0]).to.be.equal(0);

        let x = [
            {col: 1},
            {col: 2},
            {col: 3},
        ];

        const out3 = Q.join(T(x), "col").execute([]);
        expect(out3).to.be.deep.equal([]);
    });

    it("Product fluent", () => {
        const testData = ["a", "b", "c"];
        const testExpected = [
            {left: "a", right: "a"},
            {left: "a", right: "b"},
            {left: "a", right: "c"},
            {left: "b", right: "a"},
            {left: "b", right: "b"},
            {left: "b", right: "c"},
            {left: "c", right: "a"},
            {left: "c", right: "b"},
            {left: "c", right: "c"},
        ];

        const query = Q.product(new q.IdNode());
        const out = query.execute(testData);
        const len = testData.length;
        expect(out).to.have.length(len * len);
        expect(out).to.deep.equal(testExpected);
    });

    it("Presidents", () => {
        let industryTable = [
            {Name : "B. Obama", Industry: "Politics"},
            {Name : "D. Trump", Industry: "Real Estate"},
            {Name : "B. Gates", Industry: "Philanthropy"},
            {Name : "B. Obama", Industry: "Education"}
        ];

        let partyTable = [
            {Name: "B. Obama", Party: "Democrat"},
            {Name: "D. Trump", Party: "Republican"},
            {Name: "H. Clinton", Party: "Democrat"}
        ];

        let expected = [
            {Name: 'B. Obama', Industry: 'Politics', Party: 'Democrat'},
            {Name: 'B. Obama', Industry: 'Education', Party: 'Democrat'},
            {Name: 'D. Trump', Industry: 'Real Estate', Party: 'Republican'}
        ];

        const query = new q.HashJoinNode("Name" , new q.IdNode(), new Stub(partyTable));
        const out = query.execute(industryTable);

        expect(out).to.deep.equal(expected);

    });

    it("Multiple InputTables", () => {
        let salariesTable = [
            {Id : 123456, Salary: 72000, Year: 2016},
            {Id : 123456, Salary: 102000, Year: 2017},
            {Id : 654321, Salary: 42000, Year: 2017},
        ];

        let employeesTable = [
            {Id : 123456, Name: "John Doe"},
            {Id : 654321, Name: "Jane Doe"},
            {Id : 111111, Name: "John Smith"},
        ];

        let expected = [
            {Id : 123456, Name: "John Doe", Salary: 102000, Year: 2017},
            {Id : 654321, Name: "Jane Doe", Salary: 42000, Year: 2017},
        ];
        
        const query = Q.filter(x => x.Year == 2017).join(T(employeesTable), "Id");
        
        const out = query.execute(salariesTable);

        expect(out).to.deep.equal(expected);

    });

    it("Crimes on the same area on consecutive days of the week", () => {
        const rawData = crimeData.data;
        const out = queries.consecutiveCrimesPairsSamePlace.execute(rawData);
        
        let ans = [];
        let mp = queries.cleanupQuery2.execute(rawData);
        for (let x of mp) {
            for (let y of mp) {
                let d1 = new Date(x.date);
                let d2 = new Date(y.date);

                if (x.area == y.area && (d1.getDate() + 1) % 7 == d2.getDate()) {
                    ans.push(y);
                }
            }
        }
        
        out.sort(x => x.date);
        ans.sort(x => x.date);
        expect(out).to.be.deep.equal(ans);
    });

    it("Four consecutive crime days", () => {
        const rawData = crimeData.data;
        const out = queries.fourCrimeDays.execute(rawData);
        expect(out.length).to.be.equal(1);
    }).timeout(50000);


    it("Validate join-like queries are not optimized", () => {
        const query = queries.fakeJoinQuery;

        const opt = query.optimize();
        expectQuerySequence(opt, ["CartesianProduct", "Filter", "Apply"]);

        const out = opt.execute(rawData);
        expect(out).to.deep.equal(query.execute(rawData));
    });

    it("Validate (optimized) hash join is fast in the worst case", () => {
        let n = 100000;
        let a = Array.from(Array(n).keys()).map(x => ({col: x}));
        let b = Array.from(Array(n).keys()).map(x => ({col: Math.floor(n/2) + x}));
        let out = Q.join(T(b), "col").optimize().execute(a);
        expect(out.length).to.be.equal(Math.floor(n/2));
    }).timeout(1000);

    it("Fluent join methods are pure", () => {
        const data = [
            {a: 0, b: 2},
            {a: 1, b: 3},
            {a: 4, b: 6},
            {a: 5, b: 7},
            {a: 8, b: 10},
            {a: 9, b: 11},
            {a: 12, b: 13}
        ];

        const copy = JSON.parse(JSON.stringify(data));


        const query = Q.join(Q, (l, r) => l.b % 2 == r.b % 2).
                        join(Q, (l, r) => l.a % 2 == r.a % 2).
                        apply(x => ({c: x.a + x.b}));

        const out = query.execute(data);
        expect(data).to.be.deep.equal(copy);
    });

    it("Group by test", () => {
        let table = [
            {name: "B. Obama", industry: "Politics", mater: "Harvard"},
            {name: "B. Obama", industry: "Politics", mater: "Harvard"},
            {name: "B. Clinton", industry: "Politics", mater: "Yale"},
            {name: "B. Gates", industry: "Philanthropy", mater: "Harvard"}
        ];

        let expected = [
            { group: 'Politics', records: [
                {name: "B. Obama", industry: "Politics", mater: "Harvard"},
                {name: "B. Obama", industry: "Politics", mater: "Harvard"},
                {name: "B. Clinton", industry: "Politics", mater: "Yale"}
            ] },
            { group: 'Philanthropy', records: [
                {name: "B. Gates", industry: "Philanthropy", mater: "Harvard"}
            ] }
          ];

        const out = Q.group_by(x => x.industry).execute(table);
        expect(out).to.be.deep.equals(expected);
    });

    it("Reduce sum", () => {
        let n = 10;
        let a = Array.from(Array(n).keys());
        let out = Q.reduce((x, y) => x + y, 0).execute(a);
        expect(out).to.be.equal(n * (n - 1) / 2);
    });

});