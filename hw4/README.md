This directory contains a starterkit for Homework 4, "a Reactive Library".

The library skeleton is contained in src/rx.js. As with previous assignments,
we have provided a test suite for it which you can run with `npm test`
(of course once you've run `npm install` to set up the node module).

You will also implement the web interface exposed in index.html. You should
not have to modify that file directly - instead add your code to the setup($)
function at the end of src/rx.js.

You will be graded based both on the performance of your library on a standard
test suite, and on the apparent correctness of your implementation of the
web interface.
