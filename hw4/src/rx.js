class Stream {
    constructor() {
        this.callbacks = [];
    }

    static timer(N) {
        var s = new Stream();
        setInterval(function(){
            s._push(new Date());
        }, N);
        return s;
    }

    static dom(element, eventName){
        var s = new Stream();
        element.on(eventName, function(x) {
            s._push(x);
        });
        return s;
    }

    static url(url) {
        var s = new Stream();

        $.get(url, 
            function(x) {
                s._push(x);
            }, 
            "json");

        return s;
    }

    subscribe(f) {
        this.callbacks.push(f);
    }

    _push(x) {
        for (let f of this.callbacks) {
            f(x);
        }
    }

    _push_many(x) {
        for (let v of x) {
            this._push(v);
        }
    }

    __wrap_push(stream) {
        this.subscribe(x => stream._push(x));
        return stream;
    }

    first() {
        return this.__wrap_push(new StreamFirst());
    }

    map(f) {
        return this.__wrap_push(new StreamMap(f));
    }

    filter(f) {
        return this.__wrap_push(new StreamFilter(f));
    }

    distinctUntilChanged() {
        return this.__wrap_push(new StreamDistinctUntilChanged());
    }

    flatten() {
        return this.__wrap_push(new StreamFlatten());
    }

    scan(f, acc) {
        return this.__wrap_push(new StreamScan(f, acc));
    }

    join(stream) {
        return this.__wrap_push(new StreamJoin(stream));
    }

    combine() {
        return this.__wrap_push(new StreamCombine());
    }

    zip(stream, f) {
        return this.__wrap_push(new StreamZip(stream, f));
    }

    throttle(N) {
        return this.__wrap_push(new StreamThrottle(N));
    }

    latest() {
        return this.__wrap_push(new StreamLatest());
    }

    unique(f) {
        return this.__wrap_push(new StreamUnique(f));
    }

    static ajax(_url, _data) {
        var out = new Stream();
        $.ajax({
            url: _url,
            jsonp: "callback",
            dataType: "jsonp",
            data: _data,
            success: function(data) { out._push(data[1]); }
        });
        return out;
    }
}

class StreamUnique extends Stream {
    constructor(f) {
        super();
        this.f = f;
        this.prev = {};
    }

    _push(x) {
        let h = this.f(x);
        if (!this.prev.hasOwnProperty(h)) {
            super._push(x);
            this.prev[h] = x;
        }
    }
}

class StreamZip extends Stream {
    constructor(stream, f) {
        super();
        this.gotLastValue1 = false;
        this.gotLastValue2 = false;
        this.stream = stream;
        this.f = f;
        this.stream.subscribe(x => {
            this.gotLastValue2 = true;
            this.lastValue2 = x;
            this.__push_zipped();
        });
    }

    _push(x) {
        this.gotLastValue1 = true;
        this.lastValue1 = x;
        this.__push_zipped();
    }

    __push_zipped() {
        if (this.gotLastValue1 && this.gotLastValue2) {
            super._push(this.f(this.lastValue1, this.lastValue2));
        }
    }
}

class StreamCombine extends Stream {
    _push(x) {
        if (x instanceof Stream) {
            x.subscribe(y => super._push(y));
        } else {
            super._push(x);
        }
    }
}

class StreamJoin extends Stream {
    constructor(stream) {
        super();
        this.stream = stream;
        this.stream.subscribe(x => this._push(x));
    }
}

class StreamScan extends Stream {
    constructor(f, acc) {
        super();
        this.f = f;
        this.acc = acc;
    }

    _push(x) {
        this.acc = this.f(this.acc, x);
        super._push(this.acc);
    }
}

class StreamFlatten extends Stream {
    _push(x) {
        if (Array.isArray(x)) {
            for (let v of x) {
                super._push(v);
            }
        }
    }
}

class StreamDistinctUntilChanged extends Stream {
    constructor() {
        super();
        this.lastItem = undefined;
    }

    _push(x) {
        if (this.lastItem != x) {
            super._push(x);
        }

        this.lastItem = x;
    }
}

class StreamFilter extends Stream {
    constructor(f) {
        super();
        this.f = f;
    }
    
    _push(x) {
        if (this.f(x)) {
            super._push(x);
        }
    }
}

class StreamMap extends Stream {
    constructor(f) {
        super();
        this.f = f;
    }
    
    _push(x) {
        super._push(this.f(x));
    }
}

class StreamFirst extends Stream {
    constructor() {
        super();
        this.doneFirst = false;
    }

    _push(x) {
        if (this.doneFirst) {
            return;
        }
        
        this.doneFirst = true;
        for (let f of this.callbacks) {
            f(x);
        }
    }
}

class StreamThrottle extends Stream {
    constructor(N) {
        super();
        this.N = N;    
    }

    _push(x) {
        var that = this;
        setTimeout(function() {
            that._super_push(x);
        }, this.N);
    }

    _super_push(x) {
        super._push(x);
    }
}

class StreamLatest extends Stream {

    _push(x) {
        if (x instanceof Stream) {
            this.previousStream = this.latestStream;
            this.latestStream = x;
            this.latestStream.subscribe(y => this.__push(x, y));
            this.pushFlag = false;
        } 
    }

    __push(source, data) {
        // If the stream source is latest, then just push through data
        if (this.latestStream === source) {
            this.pushFlag = true;
            super._push(data);
        } else if (this.previousStream === source && !this.pushFlag) {
            super._push(data);
        }
    }
}

if (typeof exports !== "undefined") {
    exports.Stream = Stream;
}

// dependency injection let's do it
function setup($) {
    const FIRE911URL = "https://data.seattle.gov/views/kzjm-xkqj/rows.json?accessType=WEBSITE&method=getByIds&asHashes=true&start=0&length=10&meta=false&$order=:id";

    function WIKIPEDIAGET(s) {
        return Stream.ajax("https://en.wikipedia.org/w/api.php", { action: "opensearch", search: s, namespace: 0, limit: 10 });
    }

    // Add your hooks to implement Parts 2-4 here.

    // Part 2 - Question 1
    Stream.timer(1000).subscribe(function(x) {
        $("#time").text(x);
    });

    // Part 2 - Question 2
    var numOfClicks = 0;
    Stream.dom($("#button"), "click").subscribe(function(x) {
        numOfClicks++;
        $("#clicks").text("Clicked " + numOfClicks + " times");
    });

    // Part 2 - Question 3
    Stream.dom($("#mousemove"), "mousemove").throttle(1000).subscribe(function(x){
        $("#mouseposition").text(`<${x.originalEvent.pageX} , ${x.originalEvent.pageY}>`);
    });

    // Part 2 - Question 4
    // Stream.url(FIRE911URL).subscribe(function(x) {
    //     for (let f of x) {
    //         $("#fireevents").append($("<li></li>").text(`[${f["405818853"]}]\n ${f["405818852"]} - ${new Date(f["updated_at"])}`));
    //     }
    // });

    // Part 2 - Question 5
    Stream.dom($("#wikipediasearch"), "keyup").map(function(event) {
        return WIKIPEDIAGET(event.target.value).throttle(100);
    }).latest().subscribe(function(data) {
        for (let searchSuggestion of data) {
            $("#wikipediasuggestions").prepend($("<li></li>").text(searchSuggestion));
        }
    });

    $("#clear_wikipedia").on("click", x => $("#wikipediasuggestions").text(""));

    // Part 3
    let time911 = 1000; // milliseconds
    Stream.timer(time911).
        map(t => Stream.url(FIRE911URL).flatten()).
        combine().
        unique(x => x['id']).
        subscribe(f => {
            $("#fireevents").append($("<li></li>").text(`(${f['id']}) [${f["405818853"]}]\n ${f["405818852"]} - ${new Date(f["updated_at"] * 1000)}`));
        });

    Stream.dom($("#firesearch"), "input").subscribe(e => {
        let term = e.target.value.trim();

        $("#fireevents li").each((a, b) => {
            let text = b.textContent;
            if (term != "" && !text.toLowerCase().includes(term.toLowerCase())) {
                $(b).hide();
            } else {
                $(b).show();
            }
        });
    });


    // Part 4 
    var count = 0;
    var totalCount = 10;
    Stream.dom($("#load-more"), "click").join(Stream.dom($("#load-complete"), "click")).subscribe(function(x) {
        if (count == totalCount)
            return;
        
        if (x.currentTarget.id == "load-more") {
            count++;
            if (count >= totalCount) 
                totalCount *= 2;

            var w = (count/totalCount) * $(".progress-container").width();
            
            $("#progress").width(w);
        } else if (x.currentTarget.id == "load-complete") {
            count = totalCount;
            $("#progress").addClass("finished");
            $("#progress").width($(".progress-container").width());
        }
    })
}
