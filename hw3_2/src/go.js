const Compiler = require('../src/compiler').HandlebarsCompiler;

let template = `
<html><body>
<ul>{{#each episodes}}
    <li>
        <h1>{{title}}</h1>
        {{#if starring_luke}}
            <span class="note">Note: features Luke!</span>
        {{/if}}
    </li>
{{/each}}</ul>
</body></html>
`;

const compiler = new Compiler();


compiler.registerBlockHelper('id', function (ctx, body) {
    return body(ctx);
});


compiler.registerBlockHelper('contains', function (ctx, body) {
    return body(ctx);
});

f = compiler.compile(template)

const data = {
    episodes: [
        {title: "The Phantom Menace"},
        {title: "Attack of the Clones", starring_luke: false},
        {title: "Revenge of the Sith", starring_luke: true},
        {title: "A New Hope", starring_luke: true},
        {title: "The Empire Strikes Back", starring_luke: true},
        {title: "Return of the Jedi", starring_luke: true},
        {title: "The Force Awakens", starring_luke: true}
    ]
};


console.log(f(data));



console.log("")
console.log("")
console.log("")
console.log("") 