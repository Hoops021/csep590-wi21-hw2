const antlr4 = require("antlr4");
const HandlebarsLexer = require("../gen/HandlebarsLexer").HandlebarsLexer;
const HandlebarsParser = require("../gen/HandlebarsParser").HandlebarsParser;
const HandlebarsParserListener = require("../gen/HandlebarsParserListener").HandlebarsParserListener;

function escapeString(s) {
    return ("" + s).replace(/["'\\\n\r\u2028\u2029]/g, (c) => {
        switch (c) {
            case '"':
            case "'":
            case "\\":
                return "\\" + c;
            case "\n":
                return "\\n";
            case "\r":
                return "\\r";
            case "\u2028":
                return "\\u2028";
            case "\u2029":
                return "\\u2029";
        }
    });
}

function nameMangle(funcName) {
    return `__$${funcName}`;
}

function isValidJavascriptName(funcName){
    // NOTE: We do not care about reserved JS words because they will be name mangled 
    var validName = /^[$A-Z_][0-9A-Z_$]*$/i;
    return validName.test(funcName);
}

class HandlebarsCompiler extends HandlebarsParserListener {
    constructor() {
        super();
        this._inputVar = "_$ctx";
        this._outputVar = "_$result";
        this._helpers = {expr: {}, block: {}};
        this._usedHelpers = new Set();
        
        // Each block
        this.registerBlockHelper('each', function (ctx, body, iterable) {
            let result = "";
            for (let x of iterable) {
                result += body(x);
            }
            return result;
        });
        
        this.registerBlockHelper("if", function(ctx, body, args) {
            if (args) {
              return body(this);
            } else {
                return "";
            }
          });

        this.registerBlockHelper("with", function(ctx, body, field) {
            if (typeof(field) !== "string") {
                throw "With block helper failed - Field is not of type string";
            }

            if (ctx.hasOwnProperty(field)) {
                return body(ctx[field]);
            }

            throw "With block helper failed - Field DNE in context,f=" + field;
        });
    }

    registerExprHelper(name, helper) {
        if (!isValidJavascriptName(name)) {
            throw "Invalid helper name provided in registerExprHelper";
        }

        this._helpers.expr[name] = helper;
    }

    registerBlockHelper(name, helper) {
        if (!isValidJavascriptName(name)) {
            throw "Invalid helper name provided in registerBlockHelper";
        }

        this._helpers.block[name] = helper;
    }

    compile(template) {
        this._bodyStack = [];
        this.pushScope();
        this._usedHelpers.clear();

        const chars = new antlr4.InputStream(template);
        const lexer = new HandlebarsLexer(chars);
        const tokens = new antlr4.CommonTokenStream(lexer);
        const parser = new HandlebarsParser(tokens);
        parser.buildParseTrees = true;
        const tree = parser.document();
        antlr4.tree.ParseTreeWalker.DEFAULT.walk(this, tree);

        this.prependHelpers();

        let source = this.popScope();
        return new Function(this._inputVar, source);
    }

    popScope() {
        this.appendBodySource(`return ${this._outputVar};\n`);
        this.checkStack();
        return this._bodyStack.pop();
    }

    pushScope() {
        this._bodyStack.push(`var ${this._outputVar} = "";\n`);
    }

    getBodySource() {
        this.checkStack();
        return this._bodyStack[this._bodyStack.length - 1];
    }

    appendBodySource(str) {
        this.checkStack();
        this._bodyStack[this._bodyStack.length - 1] += str;
    }

    setBodySource(str) {
        this.checkStack();
        this._bodyStack[this._bodyStack.length - 1] = str;
    }

    checkStack() {
        if (this._bodyStack.length == 0) {
            throw new "Block is not correctly matched"
        }
    }

    prependHelpers() {
        var helperFuncs = "";
        for (var helperName of this._usedHelpers) {
            if (this._helpers.expr.hasOwnProperty(helperName)) {
                helperFuncs += `var ${nameMangle(helperName)} = ${this._helpers.expr[helperName].toString()};\n`;
            } else if (this._helpers.block.hasOwnProperty(helperName)) {           
                helperFuncs += `var ${nameMangle(helperName)} = ${this._helpers.block[helperName].toString()};\n`;
            } else {
                throw `Template referenced helper function=${helperName} that was never provided`;
            }
        }

        // Pre-append helper functions
        this.setBodySource(helperFuncs + this.getBodySource());
    }

    append(expr) {
        this.appendBodySource(`${this._outputVar} += ${expr};\n`);
    }

    exitRawElement(ctx) {
        this.append(`"${escapeString(ctx.getText())}"`);
    }

    exitExpressionElement(ctx) {
        if (ctx.source == null) {
            // TODO: need to loop through children???
            ctx.source = ctx.children[1].source;
        }

        this.append(ctx.source);
    }

    enterOpenBlockElement(ctx) {
        let blockName = ctx.ID().getText();
        if (!isValidJavascriptName(blockName)) {
            throw "Invalid helper name provided in template";
        }

        this._usedHelpers.add(blockName);
        this.pushScope();
    }

    exitBlockElement(ctx) {
        let s = this.popScope();

        let appName = ctx.openBlockElement().ID().getText();
        let closeName = ctx.closeBlockElement().ID().getText();
        if (appName !== closeName) {
            throw `"Block start '${appName}' does not match the block end '${closeName}'."`;
        }

        var source = `${nameMangle(appName)}(_$ctx, function (_$ctx) { ${s} }`;

        var children = ctx.openBlockElement().parameterExpElement();
        for (var i = 0; i < children.length; i++) {
            if (children[i].source != undefined) {
                source += `, ${children[i].source}`
            }
        }
        source += ");\n";

        this.append(source);

    }

    enterHelperAppElement(ctx) {
        if (!isValidJavascriptName(ctx.appName)) {
            throw "Invalid helper name provided in template";
        }

        this._usedHelpers.add(ctx.appName);
    }

    // Expected result example
    // _$result += __$makeURI(_$ctx, __$concat(_$ctx, 'cse', 401), 2017, 'Autumn');
    exitHelperAppElement(ctx) {
        var source = `${nameMangle(ctx.appName)}(_$ctx`
        for (var i = 0; i < ctx.children.length; i++) {
            if (ctx.children[i].source != undefined) {
                source += `, ${ctx.children[i].source}`
            }
        }
        source += ")";

        ctx.source = source;
    }

    //
    // Pipe context source up
    // Antlr seems to override value from grammer in weird ways
    //
    exitSubExpressionElement(ctx) {
        ctx.source = ctx.children[0].source;
    }

    exitParameterExpElement(ctx) {
        ctx.source = ctx.children[0].source;
    }

    exitParenthesizedElement(ctx) {
        if (ctx.source == null) {
            // TODO: need to loop through children???
            ctx.source = ctx.children[1].source;
        }
    }
}

exports.HandlebarsCompiler = HandlebarsCompiler;
