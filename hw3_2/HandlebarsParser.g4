parser grammar HandlebarsParser;

options { tokenVocab=HandlebarsLexer; }

document : element* EOF ;

element
    : rawElement
    | expressionElement
    | blockElement
    | commentElement
    ;

//
// HW3.2 Part 1-Task B
// 
blockElement
    : openBlockElement bodyBlockElement closeBlockElement
    ;

// [Felipe] helperAppElement was ambiguous when changing parameterExpElement+ => parameterExpElement*, which is required for stuff like {{$id}}
openBlockElement
    : START BLOCK ID parameterExpElement* END
    ;

// [TROY] From instructions
// "A body can be an arbitrary combination of raw text, elements, and comments; essentially, it is a full-fledged embedded sub-template."
bodyBlockElement
    : element* 
    ;

// HW3.2 to not enforce exactly one identifier in between to match 
// the id found in the grammer under openBlockElement->helperAppElement
closeBlockElement
    : START CLOSE_BLOCK ID END
    ;

//
// HW3.2 Part 1-Task A
// 

// [TROY] WIP? Working?
expressionElement returns [String source]
    : START subExpressionElement END // Executed via exitSubExpressionElement
    ;

subExpressionElement returns [String source]
    : helperAppElement // Executed via exitHelperAppElement
    | parameterExpElement {$source = $parameterExpElement.source;}
    ;

parameterExpElement returns [String source]
    : parenthesizedElement {$source = $parenthesizedElement.source;}
    | dataLookupElement {$source = $dataLookupElement.source;}
    // | literalElement {$source = "\"" +  $literalElement.source + "\"";}
    | literalElement {$source = $literalElement.source;}
    ;

literalElement returns [String source]
    : INTEGER {$source=eval($INTEGER.text);}
    | FLOAT {$source=eval($FLOAT.text);}
    | STRING {$source=$STRING.text;}
    ;

dataLookupElement returns [String source]
    : ID {$source="_$" + "ctx." + $ID.text;}
    ;

parenthesizedElement returns [String source]
    : OPEN_PAREN subExpressionElement CLOSE_PAREN {$source=$subExpressionElement.source;}
    ;

helperAppElement returns [String appName]
    : ID parameterExpElement+ {$appName = $ID.text;}
    ;


rawElement  : TEXT '{'?;

commentElement : START COMMENT END_COMMENT ;
