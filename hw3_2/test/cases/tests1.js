function calcGrade(ctx, grades) {
    var total = 0;
    for(var i = 0; i < grades.length; i++) {
        total += grades[i];
    }
    return total / grades.length;
}

function isEqual(ctx, field, comparison) {
    return field === comparison;
}

const ctx = {
    students:[
        {FirstName:"Jane", LastName:"Doe", State:"WA", Grades:[50, 40, 100]},
        {FirstName:"John", LastName:"Doe", State:"CA", Grades:[90, 95, 100]},
        {FirstName:"Johnny", LastName:"Rocket", State:"WA", Grades:[0, 50, 100]},
        {FirstName:"Spongebob", LastName:"SquarePants", State:"WA", Grades:[75, 85, 95]},
    ],
    meta:{
        title:"Student Directory"
    }
 };
 
 exports.helpers = [
     ["calcGrade", calcGrade],
     ["isEqual", isEqual],
 ];

 exports.blockHelpers = [];
 exports.ctx = ctx;
 exports.description = "Students Custom Test Case";
 