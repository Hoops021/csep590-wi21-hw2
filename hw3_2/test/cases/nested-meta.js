 const ctx = {
     foo: 0,
     bar: "child_0",
     child_0: {
         foo: 1,
         bar: "child_1",
         child_1: {
             foo: 2,
             bar: "child_2",
             child_2: {
                 foo: 3
             }
         }
     }
 };
 
 exports.helpers = [];
 exports.blockHelpers = [];
 exports.ctx = ctx;
 exports.description = "Nested scopes are propagated correctly by field reference";
 