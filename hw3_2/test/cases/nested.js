 const ctx = {
     foo: 0,
     bar: {
         foo: 1,
         bar: {
             foo: 2,
             bar: {
                 foo: 3
             }
         }
     }
 };
 
 exports.helpers = [];
 exports.blockHelpers = [];
 exports.ctx = ctx;
 exports.description = "Nested scopes are propagated correctly";
 