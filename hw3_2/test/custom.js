const expect = require("chai").expect;
const fs = require("fs");
const path = require("path");
const driver = require("../driver");

const caseDir = "test/cases";

describe("External Tests", () => {
    const inputFiles = fs.readdirSync(caseDir).filter((name) => name.endsWith(".input"));
    for (const inputFile of inputFiles) {
        if (inputFile.includes("headaches")){
            continue;
        }

        const [inputData, outputData, ctx, compiler, descr] = driver.setupTest(path.join(caseDir, inputFile));
        it(descr.description, () => {
            const testOutput = driver.runTest(inputData, outputData, ctx, compiler, descr, verbose=false);
            const compiled = testOutput[0];
            const expected = testOutput.length === 1 ? testOutput[0] : testOutput[1];
            expect(compiled).to.be.equal(expected, "rendered output does not match expected text from " + descr.outputFile);
        });
    }
});
